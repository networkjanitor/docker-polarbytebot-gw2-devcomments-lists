import prawcore.exceptions
import logging
import os
import sys
import configparser
import time
import datetime
from libpolarbytebot import polarbytebot

__version__ = '0.0.3'
__owner = 'Xyooz'
__source_link = 'https://gitlab.com/networkjanitor/docker-polarbytebot-gw2-devcomments-lists'
__user_agent = f'polarbytebot-gw2-devcomments-lists/{__version__} by /u/{__owner}'
__signature = f'\n' \
              f'\n' \
              f'---\n' \
              f'^(Beep boop. This message was created by a bot. Please message /u/{__owner} if you have any ' \
              f'questions, suggestions or concerns.) [^Source ^Code]({__source_link})'
__subreddits = 'guildwars2+test+gw2economy'
__microservice_id = 'gw2-devcm-list'

__anetpool_template = f'This is a list of links to comments made by ArenaNet employees in this thread:\n\n' \
                      f'&#009;' \
                      f'{__signature}\n\n' \
                      f'^(To find this post you can also search for the following keywords: developer response anet arenanet devresp)'
__anetline_template = '\n\n* [Comment by {0}]({1}?context=1000) - {2}'


def run():
    path_to_conf = os.path.abspath(os.path.dirname(sys.argv[0]))
    path_to_conf = os.path.join(path_to_conf, 'settings.conf')
    cfg = configparser.ConfigParser()
    cfg.read(path_to_conf)
    pbb = polarbytebot.Polarbytebot(signature=__signature, microservice_id=__microservice_id,
                                    oauth_client_id=cfg['oauth2']['client_id'],
                                    oauth_client_secret=cfg['oauth2']['client_secret'],
                                    oauth_redirect_uri=cfg['oauth2']['redirect_uri'],
                                    oauth_username=cfg['oauth2']['username'], praw_useragent=__user_agent,
                                    oauth_refresh_token=cfg.get('oauth2', 'refresh_token'),
                                    database_system=cfg.get('database', 'system'),
                                    database_username=cfg.get('database', 'username'),
                                    database_password=cfg.get('database', 'password'),
                                    database_host=cfg.get('database', 'host'),
                                    database_dbname=cfg.get('database', 'database'))
    while True:
        safe_poll(pbb)


def safe_poll(pbb):
    try:
        for cm in pbb.reddit.subreddit(__subreddits).stream.comments():
            if cm.author.name == pbb.username:
                continue

            name_list = pbb.guildwars2_list_developer()

            comment_list(cm, name_list, pbb)
    except prawcore.exceptions.RequestException as e:
        pass


def comment_list(cm, name_list, pbb):
    if cm.author.name in name_list:
        pbb.guildwars2_create_edit(cm.link_id, __anetline_template.format(
            cm.author.name, pbb.fix_permalink(cm.permalink),
            datetime.datetime.fromtimestamp(cm.created_utc, datetime.timezone.utc).isoformat(' ')), __anetpool_template)



if __name__ == '__main__':
    run()
