docker-polarbytebot-gw2-devcomments-lists
=========================================

* all actions to the database and all the actual logic is part of the `polarbytebot library <https://gitlab.com/networkjanitor/libpolarbytebot>`_
* this is a small microservice/dockercontainer which queues new comments into the database
* these comments are in this case lists to developer comments in their respective threads